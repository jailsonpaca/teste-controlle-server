import express from 'express';
import { post, get, remove } from './attachments';

const app = express();

const router = express.Router();

router.post('/attachments', post);
router.get('/attachments', get);
router.post('/attachments/delete', remove);
app.use('/', router);

export default app;
