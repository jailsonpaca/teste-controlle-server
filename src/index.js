import http from 'http';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import sdk from './sdk';

const app = express();

app.use(cors());
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());

const httpServer = http.createServer(sdk(app));

httpServer.listen(3001, () => {
  // eslint-disable-next-line no-console
  console.log('🚀 Server ready at http://localhost:3001');
});

export default app;
